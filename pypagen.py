import random


lowercase_letters = 'abcdefghijklmnopqrstuvwxyz'
uppercase_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
numbers = '0123456789'
special_characters = '$@;:()^&/$!%'


def gather_user_inputs():
    """gether user inputs and preferences"""

    length = int(input('Enter the desired password length: '))
    include_lowercase = input(
        'Include lowercase letters? (y/n)').lower() == 'y'
    include_uppercase = input(
        'Include uppercase letters? (y/n)').lower() == 'y'
    include_numbers = input('Include numbers? (y/n)').lower() == 'y'
    include_special = input('Include special characters? (y/n)').lower() == 'y'

    return length, include_lowercase, include_uppercase, include_numbers, include_special


password_length, include_lowercase, include_uppercase, include_numbers, include_special = gather_user_inputs()


def generate_password():
    """generate password based on user preferences"""

    character_set = ''

    if include_lowercase:
        character_set += lowercase_letters

    if include_uppercase:
        character_set += uppercase_letters

    if include_numbers:
        character_set += numbers

    if include_special:
        character_set += special_characters

    password = ''.join(random.choice(character_set)
                       for _ in range(password_length))

    return password


generated_password = generate_password()


def display_password(password):
    print('Generated Password: ', password)


display_password(generated_password)
